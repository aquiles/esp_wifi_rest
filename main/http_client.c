#include "http_client.h"

#include <string.h>
#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "cJSON.h"
#include "driver/gpio.h"

#define LED 2
#define TAG "HTTP"

//cJSON *json_obj;
char raw_request[500];

long double latitude,longitude; 

esp_err_t _http_event_handle(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGI(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_CONNECTED");
			memset(raw_request, 0, strlen(raw_request));
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGI(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_HEADER");
            printf("%.*s", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
			memcpy(raw_request,evt->data,evt->data_len);
			
			//ipstack string start with '{"l'
			if(raw_request[2] =='l')
				ipstack_request();
			else{
				//weather_request();
			}
            printf("%.*s", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}

void ipstack_request(){
	cJSON *json_obj = cJSON_Parse(raw_request);
	latitude= cJSON_GetObjectItemCaseSensitive(json_obj,"latitude")->valuedouble;
	longitude= cJSON_GetObjectItemCaseSensitive(json_obj,"longitude")->valuedouble;
	//ESP_LOGW("ipstack","%Lf e %Lf",latitude,longitude);
	cJSON_Delete(json_obj);
}

void weather_request(){
	double max_temp, min_temp,now_temp, umidity;
	cJSON *json_obj = cJSON_Parse(raw_request);
	max_temp = cJSON_GetObjectItemCaseSensitive(json_obj,"temp_max")->valuedouble;
	min_temp = cJSON_GetObjectItemCaseSensitive(json_obj,"temp_min")->valuedouble;
	now_temp = cJSON_GetObjectItemCaseSensitive(json_obj,"temp")->valuedouble;
	//umidity = 
}

void http_request(char url[]){
    esp_http_client_config_t config = {
        .url = url,
        .event_handler = _http_event_handle,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
	esp_err_t err = esp_http_client_perform(client);

	if (err == ESP_OK) {
	ESP_LOGI(TAG, "Status = %d, content_length = %d",
			esp_http_client_get_status_code(client),
			esp_http_client_get_content_length(client));
	
	//blink led in each request
	gpio_set_level(LED, 0);
	vTaskDelay(1000/portTICK_PERIOD_MS);
	gpio_set_level(LED, 1);

	}
	esp_http_client_cleanup(client);
}