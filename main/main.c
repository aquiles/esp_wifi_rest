#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "cJSON.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "wifi.h"
#include "http_client.h"
#include "freertos/semphr.h"
#include "string.h"
// #include "esp_wifi.h"
// #include "esp_event.h"
#define LED 2
#define WEATHER_KEY CONFIG_WEATHER_KEY
#define IPSTACK_KEY CONFIG_IPSTACK_KEY
//static TaskHandle_t receptorHandlerLed = NULL;
int global_led_flag = 1;
//extern cJSON *json_obj;
extern long double latitude,longitude; 

xSemaphoreHandle ledSemaphor;
xSemaphoreHandle conexaoWifiSemaphore;

void manage_led(void *params)
{
	gpio_pad_select_gpio(LED);
    gpio_set_direction(LED, GPIO_MODE_OUTPUT);
	int estado = 0;
	while(1)
	{
		while (global_led_flag)
		{
			gpio_set_level(LED, estado);
			vTaskDelay(1000 / portTICK_PERIOD_MS);
			estado = !estado;
		}
		gpio_set_level(LED, 1);
		xSemaphoreTake(ledSemaphor,portMAX_DELAY);
	}
		
}


void RealizaHTTPRequest(void * params)
{
  while(true)
  {
    if(xSemaphoreTake(conexaoWifiSemaphore, portMAX_DELAY))
    {
		char ipstack_url[240] = "", weather_url[240] = "";
		strcat(ipstack_url,"http://api.ipstack.com/189.6.26.179?access_key=");
		strcat(ipstack_url,IPSTACK_KEY);
		strcat(ipstack_url,"&output=json&fields=latitude,longitude");
		ESP_LOGI("Main Task", "Realiza HTTP Request");
		
		http_request(ipstack_url);

		sprintf(
			weather_url,
			"http://api.openweathermap.org/data/2.5/weather?lat=%Lf&lon=%Lf&appid=%s",
			latitude,
			longitude,
			WEATHER_KEY);

		http_request(weather_url);
    }
  }
}

void app_main(void)
{
	conexaoWifiSemaphore = xSemaphoreCreateBinary();
	ledSemaphor = xSemaphoreCreateBinary();
	xTaskCreate(&manage_led, "manage_led", 2048,NULL,1,NULL);
	
	// Inicializa o NVS
    esp_err_t ret = nvs_flash_init();

    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }

    ESP_ERROR_CHECK(ret);
	xTaskCreate(&RealizaHTTPRequest, "realizaHttpRequest", 4096,NULL,1,NULL);
	wifi_start();
	//fflush(stdout);
}
